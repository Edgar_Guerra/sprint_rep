﻿
namespace Poggies
{
    partial class GameForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GameForm));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.background = new System.Windows.Forms.PictureBox();
            this.stopblock = new System.Windows.Forms.PictureBox();
            this.moving3 = new System.Windows.Forms.PictureBox();
            this.mainLanding2 = new System.Windows.Forms.PictureBox();
            this.toplanding = new System.Windows.Forms.PictureBox();
            this.poFloor = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.character = new System.Windows.Forms.PictureBox();
            this.GameLoop = new System.Windows.Forms.Timer(this.components);
            this.ScrumComputer = new System.Windows.Forms.PictureBox();
            this.pictureBox15 = new System.Windows.Forms.PictureBox();
            this.DevComputer = new System.Windows.Forms.PictureBox();
            this.moving1 = new System.Windows.Forms.PictureBox();
            this.moving2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.scrumLanding = new System.Windows.Forms.PictureBox();
            this.moving4 = new System.Windows.Forms.PictureBox();
            this.moving5 = new System.Windows.Forms.PictureBox();
            this.moving6 = new System.Windows.Forms.PictureBox();
            this.moving7 = new System.Windows.Forms.PictureBox();
            this.devKeyLanding = new System.Windows.Forms.PictureBox();
            this.edgeBlock = new System.Windows.Forms.PictureBox();
            this.ProductOwnerComputer = new System.Windows.Forms.PictureBox();
            this.enemy1 = new System.Windows.Forms.PictureBox();
            this.enemy2 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.background)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stopblock)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.moving3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainLanding2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.toplanding)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.poFloor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.character)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ScrumComputer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DevComputer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.moving1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.moving2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.scrumLanding)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.moving4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.moving5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.moving6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.moving7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.devKeyLanding)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edgeBlock)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProductOwnerComputer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.enemy1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.enemy2)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 464);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(163, 31);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Tag = "ground";
            // 
            // background
            // 
            this.background.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.background.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.background.Image = ((System.Drawing.Image)(resources.GetObject("background.Image")));
            this.background.Location = new System.Drawing.Point(0, -1);
            this.background.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.background.Name = "background";
            this.background.Size = new System.Drawing.Size(1459, 469);
            this.background.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.background.TabIndex = 0;
            this.background.TabStop = false;
            this.background.Tag = "background";
            // 
            // stopblock
            // 
            this.stopblock.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("stopblock.BackgroundImage")));
            this.stopblock.Location = new System.Drawing.Point(751, 430);
            this.stopblock.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.stopblock.Name = "stopblock";
            this.stopblock.Size = new System.Drawing.Size(44, 31);
            this.stopblock.TabIndex = 1;
            this.stopblock.TabStop = false;
            this.stopblock.Tag = "ground";
            // 
            // moving3
            // 
            this.moving3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("moving3.BackgroundImage")));
            this.moving3.Location = new System.Drawing.Point(333, 351);
            this.moving3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.moving3.Name = "moving3";
            this.moving3.Size = new System.Drawing.Size(40, 31);
            this.moving3.TabIndex = 1;
            this.moving3.TabStop = false;
            this.moving3.Tag = "ground";
            // 
            // mainLanding2
            // 
            this.mainLanding2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("mainLanding2.BackgroundImage")));
            this.mainLanding2.Location = new System.Drawing.Point(515, 468);
            this.mainLanding2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.mainLanding2.Name = "mainLanding2";
            this.mainLanding2.Size = new System.Drawing.Size(171, 31);
            this.mainLanding2.TabIndex = 1;
            this.mainLanding2.TabStop = false;
            this.mainLanding2.Tag = "ground";
            // 
            // toplanding
            // 
            this.toplanding.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("toplanding.BackgroundImage")));
            this.toplanding.Location = new System.Drawing.Point(0, 181);
            this.toplanding.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.toplanding.Name = "toplanding";
            this.toplanding.Size = new System.Drawing.Size(213, 31);
            this.toplanding.TabIndex = 1;
            this.toplanding.TabStop = false;
            this.toplanding.Tag = "ground";
            // 
            // poFloor
            // 
            this.poFloor.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("poFloor.BackgroundImage")));
            this.poFloor.Location = new System.Drawing.Point(313, 81);
            this.poFloor.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.poFloor.Name = "poFloor";
            this.poFloor.Size = new System.Drawing.Size(171, 31);
            this.poFloor.TabIndex = 1;
            this.poFloor.TabStop = false;
            this.poFloor.Tag = "ground";
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox7.BackgroundImage")));
            this.pictureBox7.Location = new System.Drawing.Point(259, 263);
            this.pictureBox7.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(40, 31);
            this.pictureBox7.TabIndex = 1;
            this.pictureBox7.TabStop = false;
            this.pictureBox7.Tag = "ground";
            // 
            // character
            // 
            this.character.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("character.BackgroundImage")));
            this.character.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.character.Location = new System.Drawing.Point(57, 384);
            this.character.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.character.Name = "character";
            this.character.Size = new System.Drawing.Size(44, 63);
            this.character.TabIndex = 2;
            this.character.TabStop = false;
            this.character.Tag = "character";
            // 
            // GameLoop
            // 
            this.GameLoop.Enabled = true;
            this.GameLoop.Interval = 10;
            this.GameLoop.Tick += new System.EventHandler(this.Tick);
            // 
            // ScrumComputer
            // 
            this.ScrumComputer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ScrumComputer.Enabled = false;
            this.ScrumComputer.Image = ((System.Drawing.Image)(resources.GetObject("ScrumComputer.Image")));
            this.ScrumComputer.Location = new System.Drawing.Point(1234, 92);
            this.ScrumComputer.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ScrumComputer.Name = "ScrumComputer";
            this.ScrumComputer.Size = new System.Drawing.Size(50, 50);
            this.ScrumComputer.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.ScrumComputer.TabIndex = 3;
            this.ScrumComputer.TabStop = false;
            this.ScrumComputer.Tag = "Computer";
            this.ScrumComputer.Visible = false;
            // 
            // pictureBox15
            // 
            this.pictureBox15.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox15.BackgroundImage")));
            this.pictureBox15.Location = new System.Drawing.Point(235, 123);
            this.pictureBox15.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox15.Name = "pictureBox15";
            this.pictureBox15.Size = new System.Drawing.Size(40, 31);
            this.pictureBox15.TabIndex = 1;
            this.pictureBox15.TabStop = false;
            this.pictureBox15.Tag = "ground";
            // 
            // DevComputer
            // 
            this.DevComputer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.DevComputer.Enabled = false;
            this.DevComputer.Image = ((System.Drawing.Image)(resources.GetObject("DevComputer.Image")));
            this.DevComputer.Location = new System.Drawing.Point(1528, 26);
            this.DevComputer.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.DevComputer.Name = "DevComputer";
            this.DevComputer.Size = new System.Drawing.Size(50, 50);
            this.DevComputer.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.DevComputer.TabIndex = 3;
            this.DevComputer.TabStop = false;
            this.DevComputer.Tag = "Computer";
            this.DevComputer.Visible = false;
            // 
            // moving1
            // 
            this.moving1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("moving1.BackgroundImage")));
            this.moving1.Location = new System.Drawing.Point(810, 351);
            this.moving1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.moving1.Name = "moving1";
            this.moving1.Size = new System.Drawing.Size(87, 31);
            this.moving1.TabIndex = 1;
            this.moving1.TabStop = false;
            this.moving1.Tag = "ground";
            // 
            // moving2
            // 
            this.moving2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("moving2.BackgroundImage")));
            this.moving2.Location = new System.Drawing.Point(1045, 295);
            this.moving2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.moving2.Name = "moving2";
            this.moving2.Size = new System.Drawing.Size(85, 31);
            this.moving2.TabIndex = 1;
            this.moving2.TabStop = false;
            this.moving2.Tag = "ground";
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox3.BackgroundImage")));
            this.pictureBox3.Location = new System.Drawing.Point(235, 416);
            this.pictureBox3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(40, 31);
            this.pictureBox3.TabIndex = 1;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Tag = "ground";
            // 
            // scrumLanding
            // 
            this.scrumLanding.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("scrumLanding.BackgroundImage")));
            this.scrumLanding.Location = new System.Drawing.Point(1234, 160);
            this.scrumLanding.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.scrumLanding.Name = "scrumLanding";
            this.scrumLanding.Size = new System.Drawing.Size(96, 31);
            this.scrumLanding.TabIndex = 1;
            this.scrumLanding.TabStop = false;
            this.scrumLanding.Tag = "ground";
            // 
            // moving4
            // 
            this.moving4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("moving4.BackgroundImage")));
            this.moving4.Location = new System.Drawing.Point(1387, 382);
            this.moving4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.moving4.Name = "moving4";
            this.moving4.Size = new System.Drawing.Size(87, 31);
            this.moving4.TabIndex = 1;
            this.moving4.TabStop = false;
            this.moving4.Tag = "ground";
            // 
            // moving5
            // 
            this.moving5.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("moving5.BackgroundImage")));
            this.moving5.Location = new System.Drawing.Point(1627, 295);
            this.moving5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.moving5.Name = "moving5";
            this.moving5.Size = new System.Drawing.Size(40, 31);
            this.moving5.TabIndex = 1;
            this.moving5.TabStop = false;
            this.moving5.Tag = "ground";
            // 
            // moving6
            // 
            this.moving6.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("moving6.BackgroundImage")));
            this.moving6.Location = new System.Drawing.Point(1627, 222);
            this.moving6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.moving6.Name = "moving6";
            this.moving6.Size = new System.Drawing.Size(40, 31);
            this.moving6.TabIndex = 1;
            this.moving6.TabStop = false;
            this.moving6.Tag = "ground";
            // 
            // moving7
            // 
            this.moving7.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("moving7.BackgroundImage")));
            this.moving7.Location = new System.Drawing.Point(1627, 160);
            this.moving7.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.moving7.Name = "moving7";
            this.moving7.Size = new System.Drawing.Size(40, 31);
            this.moving7.TabIndex = 1;
            this.moving7.TabStop = false;
            this.moving7.Tag = "ground";
            // 
            // devKeyLanding
            // 
            this.devKeyLanding.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("devKeyLanding.BackgroundImage")));
            this.devKeyLanding.Location = new System.Drawing.Point(1509, 92);
            this.devKeyLanding.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.devKeyLanding.Name = "devKeyLanding";
            this.devKeyLanding.Size = new System.Drawing.Size(111, 31);
            this.devKeyLanding.TabIndex = 1;
            this.devKeyLanding.TabStop = false;
            this.devKeyLanding.Tag = "ground";
            // 
            // edgeBlock
            // 
            this.edgeBlock.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("edgeBlock.BackgroundImage")));
            this.edgeBlock.Location = new System.Drawing.Point(1885, 468);
            this.edgeBlock.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.edgeBlock.Name = "edgeBlock";
            this.edgeBlock.Size = new System.Drawing.Size(40, 31);
            this.edgeBlock.TabIndex = 1;
            this.edgeBlock.TabStop = false;
            this.edgeBlock.Tag = "ground";
            // 
            // ProductOwnerComputer
            // 
            this.ProductOwnerComputer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ProductOwnerComputer.Image = ((System.Drawing.Image)(resources.GetObject("ProductOwnerComputer.Image")));
            this.ProductOwnerComputer.Location = new System.Drawing.Point(361, 14);
            this.ProductOwnerComputer.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ProductOwnerComputer.Name = "ProductOwnerComputer";
            this.ProductOwnerComputer.Size = new System.Drawing.Size(50, 50);
            this.ProductOwnerComputer.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.ProductOwnerComputer.TabIndex = 3;
            this.ProductOwnerComputer.TabStop = false;
            this.ProductOwnerComputer.Tag = "Computer";
            // 
            // enemy1
            // 
            this.enemy1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.enemy1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.enemy1.Location = new System.Drawing.Point(587, 433);
            this.enemy1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.enemy1.Name = "enemy1";
            this.enemy1.Size = new System.Drawing.Size(29, 30);
            this.enemy1.TabIndex = 4;
            this.enemy1.TabStop = false;
            this.enemy1.Tag = "enemy";
            // 
            // enemy2
            // 
            this.enemy2.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.enemy2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.enemy2.Location = new System.Drawing.Point(88, 146);
            this.enemy2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.enemy2.Name = "enemy2";
            this.enemy2.Size = new System.Drawing.Size(29, 30);
            this.enemy2.TabIndex = 5;
            this.enemy2.TabStop = false;
            this.enemy2.Tag = "enemy";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(998, 195);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 17);
            this.label1.TabIndex = 6;
            this.label1.Text = "Careful!";
            // 
            // GameForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1539, 423);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.enemy2);
            this.Controls.Add(this.enemy1);
            this.Controls.Add(this.DevComputer);
            this.Controls.Add(this.ScrumComputer);
            this.Controls.Add(this.ProductOwnerComputer);
            this.Controls.Add(this.character);
            this.Controls.Add(this.pictureBox15);
            this.Controls.Add(this.moving4);
            this.Controls.Add(this.scrumLanding);
            this.Controls.Add(this.devKeyLanding);
            this.Controls.Add(this.moving7);
            this.Controls.Add(this.moving6);
            this.Controls.Add(this.moving5);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox7);
            this.Controls.Add(this.moving3);
            this.Controls.Add(this.edgeBlock);
            this.Controls.Add(this.stopblock);
            this.Controls.Add(this.moving2);
            this.Controls.Add(this.moving1);
            this.Controls.Add(this.mainLanding2);
            this.Controls.Add(this.poFloor);
            this.Controls.Add(this.toplanding);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.background);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "GameForm";
            this.Text = "Access each computer to move to the next room!";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.GameForm_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.GameForm_FormClosed);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyHeldDown);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPressed);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.KeyReleased);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.background)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stopblock)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.moving3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainLanding2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.toplanding)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.poFloor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.character)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ScrumComputer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DevComputer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.moving1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.moving2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.scrumLanding)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.moving4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.moving5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.moving6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.moving7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.devKeyLanding)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edgeBlock)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProductOwnerComputer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.enemy1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.enemy2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox background;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox stopblock;
        private System.Windows.Forms.PictureBox moving3;
        private System.Windows.Forms.PictureBox mainLanding2;
        private System.Windows.Forms.PictureBox toplanding;
        private System.Windows.Forms.PictureBox poFloor;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox character;
        private System.Windows.Forms.Timer GameLoop;
        private System.Windows.Forms.PictureBox ScrumComputer;
        private System.Windows.Forms.PictureBox pictureBox15;
        private System.Windows.Forms.PictureBox DevComputer;
        private System.Windows.Forms.PictureBox moving1;
        private System.Windows.Forms.PictureBox moving2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox scrumLanding;
        private System.Windows.Forms.PictureBox moving4;
        private System.Windows.Forms.PictureBox moving5;
        private System.Windows.Forms.PictureBox moving6;
        private System.Windows.Forms.PictureBox moving7;
        private System.Windows.Forms.PictureBox devKeyLanding;
        private System.Windows.Forms.PictureBox edgeBlock;
        private System.Windows.Forms.PictureBox ProductOwnerComputer;
        private System.Windows.Forms.PictureBox enemy1;
        private System.Windows.Forms.PictureBox enemy2;
        private System.Windows.Forms.Label label1;
    }
}