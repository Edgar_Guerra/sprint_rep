﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//	Project:		Sprint1
//	File Name:		DriverClass.cs
//	Description:	Acts as driver of program
//  Course:         CSCI 4250-001, Software Engineering I
//	Author:			Ian Grisham, grishami@etsu.edu, East Tennessee State University
//	Created:		March 7th, 2021
//	Copyright:		Ian Grisham, 2021
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Poggies
{

    /// <summary>
    /// DriverClass drives the program
    /// </summary>
    public static class DriverClass
    {
        public static float FinalScore { get; set; } //property for final score
        public static int time = 301;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            FinalScore = 0;
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Splash());
            Application.Run(new GameForm());
          
        }

        /// <summary>
        /// calculate the score
        /// </summary>
        /// <param name="right">correct answers</param>
        /// <param name="total">total answers</param>
        /// <param name="final">where to store to</param>
        /// <returns></returns>
        public static float CalcScore(int right, int total, float final)
        {
            if (total > 0)
            {
                float temp = (float)right / total; //get %
                temp *= 100;

                if (final == 0) //if not calc yet, set equal
                {
                    final = temp;
                } //end if

                else
                {
                    final = (float)(final + temp) / 2; //get avg
                } //end else

                
            }

            return (float)Math.Round(final, 2);

        }//end method
    }
}
