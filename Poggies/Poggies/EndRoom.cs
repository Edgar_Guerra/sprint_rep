﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//	Project:		Sprint1
//	File Name:		EndRoom.cs
//	Description:	Design and logic for the ending room
//  Course:         CSCI 4250-001, Software Engineering I
//	Author:			Jason Fields, fieldsja1@etsu.edu, East Tennessee State University
//	Created:		March 6th, 2021
//	Copyright:		Jason Fields, 2021
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Poggies
{

    /// <summary>
    /// creates end room form
    /// </summary>
    public partial class EndRoom : Form
    {
        /// <summary>
        /// Initializes a new instance of the ending room
        /// </summary>
        /// <param name="completed">The completed.</param>
        public EndRoom(string[] remaining)
        {
            InitializeComponent();
            List<string> tasksCompleted = new List<string>();
            List<string> tasksLeft = new List<string>();
            for (int i = 0; i < remaining.Length; i++)
            {
                if (remaining[i] == "")
                    tasksCompleted.Add(PoRoom.cs.Requirements[i].CorrectString);
                else
                    tasksLeft.Add(PoRoom.cs.Requirements[i].CorrectString);
            }

            //set all the text boxes
            poScoreBox.Text = PoRoom.POScore.ToString();
            tasksCompletedBox.DataSource = tasksCompleted;
            tasksLeftBox.DataSource = tasksLeft;
            smScoreBox.Text = SmRoom.SMScore.ToString();
            devScoreBox.Text = DevRoom.DEVScore.ToString(); 
            DriverClass.FinalScore = (float)Math.Round((PoRoom.POScore + SmRoom.SMScore + DevRoom.DEVScore) / 3, 2); //avg the scores

            avgScoreBox.Text = DriverClass.FinalScore.ToString();      

        }


        /// <summary>
        /// handles check button click
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        private void checkPassFailButton_Click(object sender, EventArgs e)
        {
            //if user is doing better than or equal to 70, they are passing. Failing if less.
            if (DriverClass.FinalScore >= 70)
            {
                MessageBox.Show("You're currently passing!");
            }//end if
            else
            {
                MessageBox.Show("You're currently failing!");
            }//end else
        }

        /// <summary>
        /// handles end button click
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event</param>
        private void endButton_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Sprint Completed!");
            Application.Exit();
        }


        /// <summary>
        /// handles the form closing
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EndRoom_FormClosing(object sender , FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                switch (MessageBox.Show("Are you sure you want to exit?\nYou will lose all progress" , "Warning!" , MessageBoxButtons.YesNo))
                {
                    case DialogResult.Yes:
                        Application.Exit();
                        break;
                    case DialogResult.No:
                        e.Cancel = true;
                        break;
                }
            }
        }
    }
}
