Hello, I would like some help setting up a website login page.x
I need users to register an account by entering a unique username and a password.f
The user should receive a verification email.f
The email needs to be sent to them within 5 minutes.n
Each login request should take no longer than 10 seconds.n
If the user forgot their password, they should be able reset it via email.f
Also, some type of captcha would be nice to prevent bots from brute forcing passwords.n
I think that's all for now, can you handle all of that?x
