﻿namespace Poggies
{
    partial class EndRoom
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.smScoreBox = new System.Windows.Forms.RichTextBox();
            this.poScoreBox = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.devScoreBox = new System.Windows.Forms.RichTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.avgScoreBox = new System.Windows.Forms.RichTextBox();
            this.checkPassFailButton = new System.Windows.Forms.Button();
            this.tasksLeftBox = new System.Windows.Forms.ListBox();
            this.endButton = new System.Windows.Forms.Button();
            this.tasksCompletedBox = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(35, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(172, 25);
            this.label2.TabIndex = 6;
            this.label2.Text = "Tasks Completed:";
            // 
            // smScoreBox
            // 
            this.smScoreBox.Location = new System.Drawing.Point(437, 238);
            this.smScoreBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.smScoreBox.Name = "smScoreBox";
            this.smScoreBox.ReadOnly = true;
            this.smScoreBox.Size = new System.Drawing.Size(211, 51);
            this.smScoreBox.TabIndex = 8;
            this.smScoreBox.Text = "";
            // 
            // poScoreBox
            // 
            this.poScoreBox.Location = new System.Drawing.Point(437, 106);
            this.poScoreBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.poScoreBox.Name = "poScoreBox";
            this.poScoreBox.ReadOnly = true;
            this.poScoreBox.Size = new System.Drawing.Size(211, 46);
            this.poScoreBox.TabIndex = 9;
            this.poScoreBox.Text = "";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(35, 296);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(109, 25);
            this.label1.TabIndex = 10;
            this.label1.Text = "Tasks Left:";
            // 
            // devScoreBox
            // 
            this.devScoreBox.Location = new System.Drawing.Point(433, 384);
            this.devScoreBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.devScoreBox.Name = "devScoreBox";
            this.devScoreBox.ReadOnly = true;
            this.devScoreBox.Size = new System.Drawing.Size(211, 50);
            this.devScoreBox.TabIndex = 11;
            this.devScoreBox.Text = "";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(439, 79);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(205, 25);
            this.label3.TabIndex = 12;
            this.label3.Text = "Product Owner Score:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(453, 211);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(191, 25);
            this.label4.TabIndex = 13;
            this.label4.Text = "Scrum Master Score";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(457, 357);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(158, 25);
            this.label5.TabIndex = 14;
            this.label5.Text = "Developer Score";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(719, 127);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(149, 25);
            this.label6.TabIndex = 15;
            this.label6.Text = "Average Score:";
            // 
            // avgScoreBox
            // 
            this.avgScoreBox.Location = new System.Drawing.Point(689, 156);
            this.avgScoreBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.avgScoreBox.Name = "avgScoreBox";
            this.avgScoreBox.ReadOnly = true;
            this.avgScoreBox.Size = new System.Drawing.Size(211, 50);
            this.avgScoreBox.TabIndex = 16;
            this.avgScoreBox.Text = "";
            // 
            // checkPassFailButton
            // 
            this.checkPassFailButton.Location = new System.Drawing.Point(724, 225);
            this.checkPassFailButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkPassFailButton.Name = "checkPassFailButton";
            this.checkPassFailButton.Size = new System.Drawing.Size(144, 49);
            this.checkPassFailButton.TabIndex = 17;
            this.checkPassFailButton.Text = "CHECK";
            this.checkPassFailButton.UseVisualStyleBackColor = true;
            this.checkPassFailButton.Click += new System.EventHandler(this.checkPassFailButton_Click);
            // 
            // tasksLeftBox
            // 
            this.tasksLeftBox.FormattingEnabled = true;
            this.tasksLeftBox.HorizontalScrollbar = true;
            this.tasksLeftBox.ItemHeight = 16;
            this.tasksLeftBox.Location = new System.Drawing.Point(40, 325);
            this.tasksLeftBox.Margin = new System.Windows.Forms.Padding(4);
            this.tasksLeftBox.Name = "tasksLeftBox";
            this.tasksLeftBox.Size = new System.Drawing.Size(356, 212);
            this.tasksLeftBox.TabIndex = 18;
            this.tasksLeftBox.TabStop = false;
            this.tasksLeftBox.UseTabStops = false;
            // 
            // endButton
            // 
            this.endButton.Location = new System.Drawing.Point(724, 285);
            this.endButton.Name = "endButton";
            this.endButton.Size = new System.Drawing.Size(144, 53);
            this.endButton.TabIndex = 19;
            this.endButton.Text = "COMPLETE SPRINT";
            this.endButton.UseVisualStyleBackColor = true;
            this.endButton.Click += new System.EventHandler(this.endButton_Click);
            // 
            // tasksCompletedBox
            // 
            this.tasksCompletedBox.FormattingEnabled = true;
            this.tasksCompletedBox.HorizontalScrollbar = true;
            this.tasksCompletedBox.ItemHeight = 16;
            this.tasksCompletedBox.Location = new System.Drawing.Point(40, 60);
            this.tasksCompletedBox.Margin = new System.Windows.Forms.Padding(4);
            this.tasksCompletedBox.Name = "tasksCompletedBox";
            this.tasksCompletedBox.Size = new System.Drawing.Size(356, 228);
            this.tasksCompletedBox.TabIndex = 20;
            this.tasksCompletedBox.TabStop = false;
            this.tasksCompletedBox.UseTabStops = false;
            // 
            // EndRoom
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(912, 562);
            this.Controls.Add(this.tasksCompletedBox);
            this.Controls.Add(this.endButton);
            this.Controls.Add(this.tasksLeftBox);
            this.Controls.Add(this.checkPassFailButton);
            this.Controls.Add(this.avgScoreBox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.devScoreBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.poScoreBox);
            this.Controls.Add(this.smScoreBox);
            this.Controls.Add(this.label2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MaximizeBox = false;
            this.Name = "EndRoom";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "End Room";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.EndRoom_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RichTextBox smScoreBox;
        private System.Windows.Forms.RichTextBox poScoreBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RichTextBox devScoreBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.RichTextBox avgScoreBox;
        private System.Windows.Forms.Button checkPassFailButton;
        private System.Windows.Forms.ListBox tasksLeftBox;
        private System.Windows.Forms.Button endButton;
        private System.Windows.Forms.ListBox tasksCompletedBox;
    }
}