﻿namespace Poggies
{
    partial class PoRoom
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.poNextRoomButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.clientStatementBox = new System.Windows.Forms.CheckedListBox();
            this.ProcessButton = new System.Windows.Forms.Button();
            this.assignText = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.scoreBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.correctStoriesBox = new System.Windows.Forms.ListBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.clientStatementDropDown = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(169, 25);
            this.label1.TabIndex = 1;
            this.label1.Text = "Client Information:";
            // 
            // poNextRoomButton
            // 
            this.poNextRoomButton.Enabled = false;
            this.poNextRoomButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.poNextRoomButton.Location = new System.Drawing.Point(592, 359);
            this.poNextRoomButton.Name = "poNextRoomButton";
            this.poNextRoomButton.Size = new System.Drawing.Size(112, 42);
            this.poNextRoomButton.TabIndex = 2;
            this.poNextRoomButton.Text = "Next";
            this.poNextRoomButton.UseVisualStyleBackColor = true;
            this.poNextRoomButton.Click += new System.EventHandler(this.PoNextRoomButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 225);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(402, 25);
            this.label2.TabIndex = 4;
            this.label2.Text = "Correct User Stories (seperated by new line):";
            // 
            // clientStatementBox
            // 
            this.clientStatementBox.FormattingEnabled = true;
            this.clientStatementBox.Location = new System.Drawing.Point(17, 79);
            this.clientStatementBox.Name = "clientStatementBox";
            this.clientStatementBox.Size = new System.Drawing.Size(534, 140);
            this.clientStatementBox.TabIndex = 5;
            // 
            // ProcessButton
            // 
            this.ProcessButton.Enabled = false;
            this.ProcessButton.Location = new System.Drawing.Point(592, 190);
            this.ProcessButton.Name = "ProcessButton";
            this.ProcessButton.Size = new System.Drawing.Size(112, 44);
            this.ProcessButton.TabIndex = 7;
            this.ProcessButton.Text = "Process";
            this.ProcessButton.UseVisualStyleBackColor = true;
            this.ProcessButton.Click += new System.EventHandler(this.ProcessButton_Click);
            // 
            // assignText
            // 
            this.assignText.Enabled = false;
            this.assignText.Location = new System.Drawing.Point(592, 319);
            this.assignText.Name = "assignText";
            this.assignText.Size = new System.Drawing.Size(112, 22);
            this.assignText.TabIndex = 8;
            this.assignText.TextChanged += new System.EventHandler(this.AssignText_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(624, 299);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 17);
            this.label3.TabIndex = 9;
            this.label3.Text = "F or N";
            // 
            // scoreBox
            // 
            this.scoreBox.Enabled = false;
            this.scoreBox.Location = new System.Drawing.Point(728, 210);
            this.scoreBox.Name = "scoreBox";
            this.scoreBox.ReadOnly = true;
            this.scoreBox.Size = new System.Drawing.Size(60, 22);
            this.scoreBox.TabIndex = 10;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(736, 190);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 17);
            this.label4.TabIndex = 11;
            this.label4.Text = "Score";
            // 
            // correctStoriesBox
            // 
            this.correctStoriesBox.Enabled = false;
            this.correctStoriesBox.FormattingEnabled = true;
            this.correctStoriesBox.ItemHeight = 16;
            this.correctStoriesBox.Location = new System.Drawing.Point(17, 253);
            this.correctStoriesBox.Name = "correctStoriesBox";
            this.correctStoriesBox.Size = new System.Drawing.Size(534, 148);
            this.correctStoriesBox.TabIndex = 12;
            this.correctStoriesBox.SelectedIndexChanged += new System.EventHandler(this.CorrectStoriesBox_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(571, 51);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(154, 17);
            this.label5.TabIndex = 13;
            this.label5.Text = "Select Client Statement";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(557, 153);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(224, 17);
            this.label6.TabIndex = 14;
            this.label6.Text = "Select lines that are requirements:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(589, 170);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(136, 17);
            this.label7.TabIndex = 15;
            this.label7.Text = "then press \'Process\'";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(557, 248);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(194, 51);
            this.label8.TabIndex = 16;
            this.label8.Text = "Select a requirement\r\ndetermine if that requirement\r\nis functional or non-functio" +
    "nal";
            // 
            // clientStatementDropDown
            // 
            this.clientStatementDropDown.AllowDrop = true;
            this.clientStatementDropDown.FormattingEnabled = true;
            this.clientStatementDropDown.Location = new System.Drawing.Point(583, 79);
            this.clientStatementDropDown.Name = "clientStatementDropDown";
            this.clientStatementDropDown.Size = new System.Drawing.Size(133, 24);
            this.clientStatementDropDown.TabIndex = 17;
            this.clientStatementDropDown.SelectedIndexChanged += new System.EventHandler(this.ClientStatementDropDown_SelectedIndexChanged);
            // 
            // PoRoom
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.clientStatementDropDown);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.correctStoriesBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.scoreBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.assignText);
            this.Controls.Add(this.ProcessButton);
            this.Controls.Add(this.clientStatementBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.poNextRoomButton);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "PoRoom";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Product Owner";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PoRoom_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button poNextRoomButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckedListBox clientStatementBox;
        private System.Windows.Forms.Button ProcessButton;
        private System.Windows.Forms.TextBox assignText;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox scoreBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ListBox correctStoriesBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox clientStatementDropDown;
    }
}

