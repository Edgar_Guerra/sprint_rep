﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Poggies
{
    public partial class Splash : Form
    {
        public Splash()
        {
            InitializeComponent();
        }

        private void killTimer_Tick(object sender, EventArgs e)
        {
            this.Close();
        }

        private void teamName_Click(object sender, EventArgs e)
        {

        }

        private void progressBar1_Click(object sender, EventArgs e)
        {
            
        }

        private void progressTimer_Tick(object sender, EventArgs e)
        {
            progressBar1.Increment(12);
        }
    }
}
