﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//	Project:		Sprint1
//	File Name:		SmRoom.cs
//	Description:	Design and logic for the scrum master room.
//  Course:         CSCI 4250-001, Software Engineering I
//	Author:			Jason Fields, fieldsja1@etsu.edu, East Tennessee State University
//	Created:		March 6th, 2021
//	Copyright:		Jason Fields, 2021
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Poggies
{

    /// <summary>
    /// creates scrum master form
    /// </summary>
    public partial class SmRoom : Form
    {

        public int reqID;
        public List<Developer> devList = new List<Developer>();
        public static string[] Rooms { get; set; }
        public int assigned;
        public static float SMScore { get; set; }

        /// <summary>
        /// Initializes a new instance of scrum master room
        /// </summary>
        public SmRoom()
        {
            InitializeComponent();
            Random rng = new Random();
            //Create the developers 
            for (int i = 0; i < 3; i++)
            {
                Developer dev = new Developer(rng.Next(1, 10), rng.Next(10, 40), i);// assign skill and experience using rng
                devList.Add(dev);// add the dev to the list
            }
            this.developersList.DataSource = devList;
            foreach (var item in PoRoom.cs.Requirements)                //for each requirement in the list
            {
                storiesList.Items.Add(item.CorrectString);      //add requirement to the list box
            }
            numTasksTextBox.Text = PoRoom.cs.Requirements.Count().ToString();      //number of tasks
            assigned = 0;
        }

        /// <summary>
        /// Handles the Click event of the SmNext control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void SmNext_Click(object sender, EventArgs e)
        {
            Rooms = new string[PoRoom.cs.Requirements.Count];
            for (int i = 1; i < PoRoom.cs.Requirements.Count + 1; i++)
                Rooms[i - 1] = i.ToString();


            this.Hide();                                          //hide this form
            SMScore = DriverClass.CalcScore(assigned, PoRoom.cs.Requirements.Count, SMScore);
                                                     
        }

        private void storiesList_SelectedIndexChanged(object sender, EventArgs e)
        {
            reqID = storiesList.SelectedIndex;
            cmbAssign.Enabled = true;
            storiesList.Enabled = false;
            cmbAssign.Visible = true;
        }

        private void developersList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!devList[developersList.SelectedIndex].myTask.Contains(PoRoom.cs.Requirements[reqID]))
            {
                devList[developersList.SelectedIndex].myTask.Add(PoRoom.cs.Requirements[reqID]);// add the selected developer to the requirement
                assigned++;
            }
            developersList.Enabled = false;
            storiesList.Enabled = true;

        }

        /// <summary>
        /// handles the form closing
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SmRoom_FormClosing(object sender , FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                switch (MessageBox.Show("Are you sure you want to exit?\nYou will lose all progress" , "Warning!" , MessageBoxButtons.YesNo))
                {
                    case DialogResult.Yes:
                        Application.Exit();
                        break;
                    case DialogResult.No:
                        e.Cancel = true;
                        break;
                }
            }
        }

        private void cmbAssign_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbAssign.SelectedIndex == 0)
            {
                storiesList.Enabled = false;
                developersList.Enabled = true;
                cmbAssign.Enabled = false;
                cmbAssign.Visible = false;
                developersList.Visible = true;
            }
            else
            {
                developersList.Enabled = false;
                developersList.Visible = false;
                storiesList.Enabled = false;
                storyPointsCombo.Enabled = true;
                storyPointsCombo.Visible = true;
                cmbAssign.Enabled = false;
                cmbAssign.Visible = false;
                storyName.Text = PoRoom.cs.Requirements[reqID].Name;
                storyPointValue.Value = PoRoom.cs.Requirements[reqID].StoryPoints;
                     /// assign story goes here
            }
        }

        private void assignPointsBtn_Click(object sender, EventArgs e)
        {
            PoRoom.cs.Requirements[reqID].StoryPoints = decimal.ToInt32(storyPointValue.Value);
            storiesList.Enabled = true;
            storyPointsCombo.Enabled = false;
            storyPointsCombo.Visible = false;
        }
    }
}
