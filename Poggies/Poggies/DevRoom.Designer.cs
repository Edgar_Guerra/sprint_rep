﻿namespace Poggies
{
    partial class DevRoom
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.roomTask = new System.Windows.Forms.RichTextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.remainingRoomsBox = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.taskCompleteCheck = new System.Windows.Forms.Button();
            this.sprintTimer = new System.Windows.Forms.Timer(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.doneButton = new System.Windows.Forms.Button();
            this.devCountdownTimer = new System.Windows.Forms.Timer(this.components);
            this.countdownDisplay = new System.Windows.Forms.Label();
            this.timeLeft = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // roomTask
            // 
            this.roomTask.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.roomTask.Location = new System.Drawing.Point(69, 57);
            this.roomTask.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.roomTask.Name = "roomTask";
            this.roomTask.ReadOnly = true;
            this.roomTask.Size = new System.Drawing.Size(681, 126);
            this.roomTask.TabIndex = 0;
            this.roomTask.Text = "";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(581, 316);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(171, 53);
            this.button1.TabIndex = 1;
            this.button1.Text = "Go to Selected Room";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // remainingRoomsBox
            // 
            this.remainingRoomsBox.FormattingEnabled = true;
            this.remainingRoomsBox.ItemHeight = 16;
            this.remainingRoomsBox.Location = new System.Drawing.Point(15, 276);
            this.remainingRoomsBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.remainingRoomsBox.Name = "remainingRoomsBox";
            this.remainingRoomsBox.Size = new System.Drawing.Size(143, 164);
            this.remainingRoomsBox.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 237);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(182, 34);
            this.label1.TabIndex = 4;
            this.label1.Text = "Select from list of rooms left\r\nor hit done to end sprint:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(65, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(132, 25);
            this.label2.TabIndex = 6;
            this.label2.Text = "Current Task:";
            // 
            // taskCompleteCheck
            // 
            this.taskCompleteCheck.Location = new System.Drawing.Point(579, 259);
            this.taskCompleteCheck.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.taskCompleteCheck.Name = "taskCompleteCheck";
            this.taskCompleteCheck.Size = new System.Drawing.Size(171, 53);
            this.taskCompleteCheck.TabIndex = 7;
            this.taskCompleteCheck.Text = "Check Task";
            this.taskCompleteCheck.UseVisualStyleBackColor = true;
            this.taskCompleteCheck.Click += new System.EventHandler(this.TaskCompleteCheck_Click);
            // 
            // sprintTimer
            // 
            this.sprintTimer.Interval = 60000;
            this.sprintTimer.Tick += new System.EventHandler(this.sprintTimer_Tick);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(10, 197);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(176, 25);
            this.label3.TabIndex = 8;
            this.label3.Text = "Rooms Remaining:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(578, 237);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(174, 17);
            this.label4.TabIndex = 9;
            this.label4.Text = "Check for task completion:";
            // 
            // doneButton
            // 
            this.doneButton.Location = new System.Drawing.Point(581, 373);
            this.doneButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.doneButton.Name = "doneButton";
            this.doneButton.Size = new System.Drawing.Size(171, 53);
            this.doneButton.TabIndex = 10;
            this.doneButton.Text = "Finish Sprint";
            this.doneButton.UseVisualStyleBackColor = true;
            this.doneButton.Click += new System.EventHandler(this.DoneButton_Click);
            // 
            // devCountdownTimer
            // 
            this.devCountdownTimer.Enabled = true;
            this.devCountdownTimer.Interval = 1000;
            this.devCountdownTimer.Tick += new System.EventHandler(this.DevCountdownTimer_Tick);
            // 
            // countdownDisplay
            // 
            this.countdownDisplay.AutoSize = true;
            this.countdownDisplay.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.countdownDisplay.Location = new System.Drawing.Point(497, 276);
            this.countdownDisplay.Name = "countdownDisplay";
            this.countdownDisplay.Size = new System.Drawing.Size(0, 36);
            this.countdownDisplay.TabIndex = 11;
            // 
            // timeLeft
            // 
            this.timeLeft.AutoSize = true;
            this.timeLeft.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeLeft.Location = new System.Drawing.Point(228, 276);
            this.timeLeft.Name = "timeLeft";
            this.timeLeft.Size = new System.Drawing.Size(154, 36);
            this.timeLeft.TabIndex = 12;
            this.timeLeft.Text = "Time Left:";
            // 
            // DevRoom
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.timeLeft);
            this.Controls.Add(this.countdownDisplay);
            this.Controls.Add(this.doneButton);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.taskCompleteCheck);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.remainingRoomsBox);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.roomTask);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MaximizeBox = false;
            this.Name = "DevRoom";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Developer Room";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DevRoom_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox roomTask;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ListBox remainingRoomsBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button taskCompleteCheck;
        private System.Windows.Forms.Timer sprintTimer;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button doneButton;
        private System.Windows.Forms.Timer devCountdownTimer;
        private System.Windows.Forms.Label countdownDisplay;
        private System.Windows.Forms.Label timeLeft;
    }
}