﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//	Project:		Sprint2
//	File Name:		GameForm.cs
//	Description:	Minigame to access the different rooms
//  Course:         CSCI 4250-001, Software Engineering I
//	Author:			Ian Grisham, grishami@etsu.edu, East Tennessee State University
//	Created:		March 27th, 2021
//	Copyright:		Ian Grisham, 2021
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Windows.Forms;

namespace Poggies
{

    /// <summary>
    /// gameform class lets user run around to access next rooms
    /// </summary>
    public partial class GameForm : Form
    {
     
        int displacement;   //how far to displace the character
        int scrollSpeed;    //how far to displace the background
        int jumpHeight;     //how high user can jump
        int gravity;        //gravity for player so they dont float

        
        //for checking the current movement of player
        bool left;
        bool right;
        bool airborne;

        //moving platform speeds
        int obstacleSpeed;
        int obstacleSpeed2;
        int obstacleSpeed3;
        int obstacleSpeed4;
        int obstacleSpeed5;
        int obstacleSpeed6;
        int obstacleSpeed7;


        /// <summary>
        /// constructor initializes
        /// </summary>
        public GameForm()
        {
            InitializeComponent();
            left = false;
            right = false;
            airborne = false;
            jumpHeight = 10;
            gravity = 3;
            displacement = 3;
            scrollSpeed = 3;
            obstacleSpeed = 2;
            obstacleSpeed2 = 2;
            obstacleSpeed3 = 1;
            obstacleSpeed4 = 1;
            obstacleSpeed5 = 1;
            obstacleSpeed6 = 2;
            obstacleSpeed7 = 3;
        }


        /// <summary>
        /// main game loop constantly updates movement of player and objects
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Tick(object sender, EventArgs e)
        {
            //initialize character gravity and obstacle speeds
            InitializeObjects();

            //if key is left or right, move the character 
            MoveCharacter();

            //move the background with character if at the edges
            MoveBackground();

            //check for airborne logic
            CheckAirborne();

            //character and ground interactions logic
            CharacterInteractions();

        }

        /// <summary>
        /// sets player gravity and starts moving the objects
        /// </summary>
        private void InitializeObjects()
        {
            character.Top += gravity;

            //start moving the obstacles
            moving1.Left -= obstacleSpeed;
            moving2.Left += obstacleSpeed;
            moving3.Left += obstacleSpeed3;
            moving4.Left += obstacleSpeed4;
            moving5.Left += obstacleSpeed5;
            moving6.Left += obstacleSpeed6;
            moving7.Left += obstacleSpeed7;
            enemy1.Left += obstacleSpeed;
            enemy2.Left += obstacleSpeed;
        }


        /// <summary>
        /// checks to see if the user is touching ground or has acquired a key
        /// </summary>
        private void CharacterInteractions()
        {
            foreach (Control control in this.Controls)
            {
                string name = (string)control.Name;


                if ((string)control.Tag == "ground" && character.Bounds.IntersectsWith(control.Bounds) && !airborne) //if touching the ground
                {
                    
                    gravity = 3;
                    jumpHeight = 10;
                    character.Top = control.Top - character.Height; //put the character on the ground
                    control.BringToFront(); //make sure character isnt overlapping                                  
                    CheckMovement(name);

                } //end if

                //if user accessed a computer
                if ((string)control.Tag == "Computer" && character.Bounds.IntersectsWith(control.Bounds) && control.Visible)
                {

                    control.Enabled = false;
                    control.Visible = false;

                    WhichComputer(name);

                } //end if

                if ((string)control.Tag == "enemy" && character.Bounds.IntersectsWith(control.Bounds))
                {
                    Death();

                } //end if

            } //end foreach

            UpdatePlatforms();
            CheckDeath();
        }


        /// <summary>
        /// reacts to whether or not user is jumping
        /// </summary>
        private void CheckAirborne()
        {
            if (airborne)
            {
                gravity = -3; //let the player leave the ground
                jumpHeight -= 1; //see how long they are jumping

                if (jumpHeight < 0) //if done jumping allowed amount
                {
                    airborne = false; //stop jumping
               
                } //end if

            } //end if

            else //otherwise stay grounded
            {
                gravity = 3;

            } //end else

        }


        /// <summary>
        /// move the background left or right depending on user input
        /// </summary>
        private void MoveBackground()
        {
            if (left && background.Left < 0)
            {
                background.Left += scrollSpeed;
                ScrollBackground(1);

            }//end if

            if (right && background.Left > this.ClientSize.Width - background.Width)
            {
                background.Left -= scrollSpeed;
                ScrollBackground(-1); //swap the sign inside method

            } //end if

        }


        /// <summary>
        /// move the character depending on key press
        /// </summary>
        private void MoveCharacter()
        {
            if (left && character.Left > 50)
            {
                character.Left -= displacement; //move left
            }
            if (right && character.Right < this.ClientSize.Width - 50)
            {
                character.Left += displacement; //move right
            }
        }


        /// <summary>
        /// check which key is being pressed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void KeyHeldDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Left)
            {
                left = true;

            } //end if 

            if(e.KeyCode == Keys.Right)
            {
                right = true;

            } //end if

        }


        /// <summary>
        /// checks to see if user is done pressing the key
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void KeyReleased(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Left)
            {
                left = false;

            } //end if

            if (e.KeyCode == Keys.Right)
            {
                right = false;

            } //end if
           

        }


        /// <summary>
        /// swaps direction of scroll based on input
        /// </summary>
        /// <param name="leftOrRight"></param>
        private void ScrollBackground(int leftOrRight)
        {
            foreach(Control control in this.Controls)
            {
                if((string)control.Tag != "character")
                {
                    control.Left += (leftOrRight * scrollSpeed); //swap the sign

                } //end if

            } //end foreach

        }


        //if user falls off 
        private void Restart()
        {
            this.Hide();
            GameForm gf = new GameForm();
            gf.Show();
        }

        private void GameForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }


        /// <summary>
        /// if player is on a platform, move with the platform
        /// </summary>
        /// <param name="txt"></param>
        private void CheckMovement(string txt)
        {

            //if on specified platform, move the character with it
            if (txt == "moving3" && !left && !right)
            {
                character.Left += obstacleSpeed3;

            } //end if

            if (txt == "moving4" && !left && !right)
            {
                character.Left += obstacleSpeed4;

            } //end if

            if (txt == "moving5" && !left && !right)
            {
                character.Left += obstacleSpeed5;

            } //end if

            if (txt == "moving6" && !left && !right)
            {
                character.Left += obstacleSpeed6;

            } //end if

            if (txt == "moving7" && !left && !right)
            {
                character.Left += obstacleSpeed7;

            } //end if
        }


        /// <summary>
        /// swaps platform direction if its movement boundary is reached
        /// </summary>
        private void UpdatePlatforms()
        {
            if (moving1.Left < mainLanding2.Left|| moving1.Right > scrumLanding.Left)
            {
                obstacleSpeed = -obstacleSpeed;

            } //end if

            if (moving2.Right < moving1.Left || moving2.Right > scrumLanding.Right)
            {
                obstacleSpeed2 = -obstacleSpeed2;

            } //end if

            if (moving3.Left < poFloor.Left || moving3.Right > mainLanding2.Left)
            {
                obstacleSpeed3 = -obstacleSpeed3;

            } //end if

            if (moving4.Left < ScrumComputer.Right || moving4.Right > edgeBlock.Left)
            {
                obstacleSpeed4 = -obstacleSpeed4;

            } //end if

            if (moving5.Left < devKeyLanding.Right || moving5.Right > edgeBlock.Left)
            {
                obstacleSpeed5 = -obstacleSpeed5;

            } //end if

            if (moving6.Left < devKeyLanding.Right || moving6.Right > edgeBlock.Left)
            {
                obstacleSpeed6 = -obstacleSpeed6;

            } //end if

            if (moving7.Left < devKeyLanding.Right || moving7.Right > edgeBlock.Left)
            {
                obstacleSpeed7 = -obstacleSpeed7;

            } //end if

            if (enemy1.Left < mainLanding2.Left || enemy1.Right > mainLanding2.Right)
            {
                obstacleSpeed = -obstacleSpeed;

            } //end if

            if (enemy2.Left < toplanding.Left || enemy2.Right > toplanding.Right)
            {
                obstacleSpeed = -obstacleSpeed;

            } //end if
        }

        /// <summary>
        /// checks if user is still on map
        /// </summary>
        private void CheckDeath()
        {
            if (character.Top > this.ClientSize.Height) //if user fell off
            {
                Death();

            } //end if 

        }

        private void Death()
        {
            GameLoop.Stop();
            MessageBox.Show("FAILURE!");
            Restart();

        }


        /// <summary>
        /// which key determines action based on computer that was accessed
        /// </summary>
        /// <param name="txt"></param>
        private void WhichComputer(string txt)
        {
            left = false;
            airborne = false;
            right = false;

            //grab key, disable this key, enable next key

            if (txt == "ProductOwnerComputer")
            {
                PoRoom po = new PoRoom();
                po.Show();
                ScrumComputer.Visible = true;
                ScrumComputer.Enabled = true;

            } //end if

            if (txt == "ScrumComputer")
            {
                SmRoom sm = new SmRoom();
                sm.Show();
                DevComputer.Visible = true;
                DevComputer.Enabled = true;

            } //end if

            if (txt == "DevComputer")
            {
                DevRoom dev = new DevRoom(0, SmRoom.Rooms, 0);
                dev.Show();
                DevComputer.Visible = false;
                DevComputer.Enabled = false;
            
            } //end if

        }

        private void GameForm_FormClosing(object sender , FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                switch (MessageBox.Show("Are you sure you want to exit?\nYou will lose all progress" , "Warning!" , MessageBoxButtons.YesNo))
                {
                    case DialogResult.Yes:
                        Application.Exit();
                        break;
                    case DialogResult.No:
                        e.Cancel = true;
                        break;
                }
            }
        }

        private void KeyPressed(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == ' ' && jumpHeight == 10)
            {
                airborne = true;

            } //end if
        }
    }
}
