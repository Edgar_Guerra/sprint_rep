﻿namespace Poggies
{
    partial class SmRoom
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.smNext = new System.Windows.Forms.Button();
            this.storiesList = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.numTasksTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.listSubTxt = new System.Windows.Forms.Label();
            this.developersList = new System.Windows.Forms.ListBox();
            this.cmbAssign = new System.Windows.Forms.ComboBox();
            this.storyPointsCombo = new System.Windows.Forms.GroupBox();
            this.assignPointsBtn = new System.Windows.Forms.Button();
            this.storyPointValue = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.storyName = new System.Windows.Forms.TextBox();
            this.StoryLabel = new System.Windows.Forms.Label();
            this.storyPointsCombo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.storyPointValue)).BeginInit();
            this.SuspendLayout();
            // 
            // smNext
            // 
            this.smNext.Location = new System.Drawing.Point(505, 385);
            this.smNext.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.smNext.Name = "smNext";
            this.smNext.Size = new System.Drawing.Size(283, 53);
            this.smNext.TabIndex = 1;
            this.smNext.TabStop = false;
            this.smNext.Text = "Done";
            this.smNext.UseVisualStyleBackColor = true;
            this.smNext.Click += new System.EventHandler(this.SmNext_Click);
            // 
            // storiesList
            // 
            this.storiesList.FormattingEnabled = true;
            this.storiesList.ItemHeight = 16;
            this.storiesList.Location = new System.Drawing.Point(13, 78);
            this.storiesList.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.storiesList.Name = "storiesList";
            this.storiesList.Size = new System.Drawing.Size(360, 356);
            this.storiesList.TabIndex = 2;
            this.storiesList.SelectedIndexChanged += new System.EventHandler(this.storiesList_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(181, 25);
            this.label2.TabIndex = 5;
            this.label2.Text = "List of User Stories:";
            // 
            // numTasksTextBox
            // 
            this.numTasksTextBox.Location = new System.Drawing.Point(668, 351);
            this.numTasksTextBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.numTasksTextBox.Name = "numTasksTextBox";
            this.numTasksTextBox.ReadOnly = true;
            this.numTasksTextBox.Size = new System.Drawing.Size(120, 22);
            this.numTasksTextBox.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(501, 353);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(137, 20);
            this.label1.TabIndex = 6;
            this.label1.Text = "Number of tasks:";
            // 
            // listSubTxt
            // 
            this.listSubTxt.AutoSize = true;
            this.listSubTxt.Location = new System.Drawing.Point(16, 53);
            this.listSubTxt.Name = "listSubTxt";
            this.listSubTxt.Size = new System.Drawing.Size(180, 17);
            this.listSubTxt.TabIndex = 7;
            this.listSubTxt.Text = "Click to Assign a Developer";
            // 
            // developersList
            // 
            this.developersList.FormattingEnabled = true;
            this.developersList.ItemHeight = 16;
            this.developersList.Location = new System.Drawing.Point(379, 78);
            this.developersList.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.developersList.Name = "developersList";
            this.developersList.Size = new System.Drawing.Size(409, 180);
            this.developersList.TabIndex = 8;
            this.developersList.Visible = false;
            this.developersList.SelectedIndexChanged += new System.EventHandler(this.developersList_SelectedIndexChanged);
            // 
            // cmbAssign
            // 
            this.cmbAssign.FormattingEnabled = true;
            this.cmbAssign.Items.AddRange(new object[] {
            "Assign Developer",
            "Assign Story Points"});
            this.cmbAssign.Location = new System.Drawing.Point(211, 78);
            this.cmbAssign.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cmbAssign.Name = "cmbAssign";
            this.cmbAssign.Size = new System.Drawing.Size(160, 24);
            this.cmbAssign.TabIndex = 9;
            this.cmbAssign.Visible = false;
            this.cmbAssign.SelectedIndexChanged += new System.EventHandler(this.cmbAssign_SelectedIndexChanged);
            // 
            // storyPointsCombo
            // 
            this.storyPointsCombo.Controls.Add(this.assignPointsBtn);
            this.storyPointsCombo.Controls.Add(this.storyPointValue);
            this.storyPointsCombo.Controls.Add(this.label3);
            this.storyPointsCombo.Controls.Add(this.storyName);
            this.storyPointsCombo.Controls.Add(this.StoryLabel);
            this.storyPointsCombo.Enabled = false;
            this.storyPointsCombo.Location = new System.Drawing.Point(379, 78);
            this.storyPointsCombo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.storyPointsCombo.Name = "storyPointsCombo";
            this.storyPointsCombo.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.storyPointsCombo.Size = new System.Drawing.Size(411, 181);
            this.storyPointsCombo.TabIndex = 10;
            this.storyPointsCombo.TabStop = false;
            this.storyPointsCombo.Text = "Assign Story Points";
            this.storyPointsCombo.Visible = false;
            // 
            // assignPointsBtn
            // 
            this.assignPointsBtn.Location = new System.Drawing.Point(12, 124);
            this.assignPointsBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.assignPointsBtn.Name = "assignPointsBtn";
            this.assignPointsBtn.Size = new System.Drawing.Size(389, 34);
            this.assignPointsBtn.TabIndex = 11;
            this.assignPointsBtn.TabStop = false;
            this.assignPointsBtn.Text = "Assign Story Points";
            this.assignPointsBtn.UseVisualStyleBackColor = true;
            this.assignPointsBtn.Click += new System.EventHandler(this.assignPointsBtn_Click);
            // 
            // storyPointValue
            // 
            this.storyPointValue.Location = new System.Drawing.Point(107, 78);
            this.storyPointValue.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.storyPointValue.Name = "storyPointValue";
            this.storyPointValue.Size = new System.Drawing.Size(295, 22);
            this.storyPointValue.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 78);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 17);
            this.label3.TabIndex = 3;
            this.label3.Text = "Story Points:";
            // 
            // storyName
            // 
            this.storyName.Location = new System.Drawing.Point(107, 28);
            this.storyName.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.storyName.Name = "storyName";
            this.storyName.ReadOnly = true;
            this.storyName.Size = new System.Drawing.Size(293, 22);
            this.storyName.TabIndex = 1;
            // 
            // StoryLabel
            // 
            this.StoryLabel.AutoSize = true;
            this.StoryLabel.Location = new System.Drawing.Point(11, 32);
            this.StoryLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.StoryLabel.Name = "StoryLabel";
            this.StoryLabel.Size = new System.Drawing.Size(90, 17);
            this.StoryLabel.TabIndex = 0;
            this.StoryLabel.Text = "Story Name: ";
            // 
            // SmRoom
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.storyPointsCombo);
            this.Controls.Add(this.cmbAssign);
            this.Controls.Add(this.developersList);
            this.Controls.Add(this.listSubTxt);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.storiesList);
            this.Controls.Add(this.smNext);
            this.Controls.Add(this.numTasksTextBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MaximizeBox = false;
            this.Name = "SmRoom";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Scrum Master";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SmRoom_FormClosing);
            this.storyPointsCombo.ResumeLayout(false);
            this.storyPointsCombo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.storyPointValue)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button smNext;
        private System.Windows.Forms.ListBox storiesList;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox numTasksTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label listSubTxt;
        private System.Windows.Forms.ListBox developersList;
        private System.Windows.Forms.ComboBox cmbAssign;
        private System.Windows.Forms.GroupBox storyPointsCombo;
        private System.Windows.Forms.TextBox storyName;
        private System.Windows.Forms.Label StoryLabel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button assignPointsBtn;
        private System.Windows.Forms.NumericUpDown storyPointValue;
    }
}