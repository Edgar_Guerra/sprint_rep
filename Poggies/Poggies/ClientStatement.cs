﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//	Project:		Sprint1
//	File Name:		ClientStatement.cs
//	Description:	Creates requirement list from file chosen by user
//  Course:         CSCI 4250-001, Software Engineering I
//	Author:			Ian Grisham, grishami@etsu.edu, East Tennessee State University
//	Created:		March 7th, 2021
//	Copyright:		Ian Grisham, 2021
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;

namespace Poggies
{

    /// <summary>
    /// class creates list of requirements
    /// </summary>
    public class ClientStatement
    {
        public List<Requirement> Requirements { get; set; } //main list used in program
        private bool IsEmpty = false;

        /// <summary>
        /// default constructor
        /// </summary>
        public ClientStatement(int index)
        {
            Requirements = new List<Requirement>();
            ReadClientStatement(index);
        }

        private void ReadClientStatement(int index)
        {
            var assembly = Assembly.GetExecutingAssembly();
            if (index == 0)
            {
                Stream stream = assembly.GetManifestResourceStream("Poggies.Resources.ClientStatement1.txt");
                StreamReader sr = new StreamReader(stream);

                //while there are lines left
                while (sr.Peek() != -1)
                {
                    Requirements.Add(new Requirement(sr.ReadLine())); //make new requirement for each line

                } //end while
            }
            else
            {
                Stream stream = assembly.GetManifestResourceStream("Poggies.Resources.ClientStatement2.txt");
                StreamReader sr = new StreamReader(stream);

                //while there are lines left
                while (sr.Peek() != -1)
                {
                    Requirements.Add(new Requirement(sr.ReadLine())); //make new requirement for each line

                } //end while
            }
        }
        /// <summary>
        /// choose and open file
        /// </summary>
        /// <returns> the chosen file </returns>
        public OpenFileDialog GetFile()
        {
            //get the file
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.InitialDirectory = Application.StartupPath;
            dlg.Title = "Select File";
            dlg.Filter = "text files (*.txt)|*.txt";

            if (dlg.ShowDialog() == DialogResult.Cancel) //if they dont chose close
            {
                MessageBox.Show("File not opened." , "File Open Error");
                IsEmpty = true;
            }

            return dlg;
        }

        /// <summary>
        /// Method breaks down file into requirement list
        /// </summary>
        /// <param name="dlg"></param>
        /// <returns></returns>
        public List<Requirement> Decompose(OpenFileDialog dlg)
        {
            StreamReader reader = null; //initialize null reader

            try
            {
                if (IsEmpty == false)
                {
                    reader = new StreamReader(dlg.FileName); //read from chosen file name

                    //while there are lines left
                    while (reader.Peek() != -1)
                    {
                        Requirements.Add(new Requirement(reader.ReadLine())); //make new requirement for each line

                    } //end while
                }
            } //end try
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            } //end catch

            finally //if all else fails
            {
                if (reader != null) //if reader was initialized
                {
                    reader.Close(); //close it

                } //end if

            } //end finally

            return Requirements;

        }//end method

    }//end class
}
