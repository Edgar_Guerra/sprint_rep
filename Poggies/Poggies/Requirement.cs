﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//	Project:		Sprint1
//	File Name:		Requirement.cs
//	Description:	
//  Course:         CSCI 4250-001, Software Engineering I
//	Author:			Ian Grisham, grishami@etsu.edu, East Tennessee State University
//	Created:		March 7th, 2021
//	Copyright:		Ian Grisham, 2021
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poggies
{
    /// <summary>
    /// Requirement class holds each requirement and right/user answers
    /// </summary>
    public class Requirement
    {
        public string MyAnswer { get; set; } //property for my answer
        public string CorrectAnswer { get; set; } //property for the correct answer to check against
        public string CorrectString { get; set; }
        public string Name { get; set; } //property for the actual string
        public int StoryPoints { get; set; } // property for Story Points
        public bool isCompleted { get; set; }

        /// <summary>
        /// constructor initializes
        /// </summary>
        /// <param name="name"></param>
        public Requirement(string name)
        {
            isCompleted = false;

            StoryPoints = 0;

            if (name.EndsWith("n") || name.EndsWith("f")) {
                this.Name = name.Substring(0 , name.Length - 1); //set this name to string minus last character
                this.CorrectAnswer = name[name.Length - 1].ToString(); //store hardcoded last character on line as answer 
                this.MyAnswer = null; //set this answer to null because user hasnt given answer yet
                this.CorrectString = name.Substring(0 , name.Length - 1);
            } //end if
            else
            {
                this.Name = name.Substring(0 , name.Length - 1); //set this name to string minus last character
                this.CorrectAnswer = name[name.Length - 1].ToString(); //store hardcoded last character on line as answer 
                this.MyAnswer = null; //set this answer to null because user hasnt given answer yet
            } //end else

        }//end constructor

    } //end requirement class
}
