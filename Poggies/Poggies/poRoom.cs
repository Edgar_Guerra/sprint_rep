﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//	Project:		Sprint1
//	File Name:		PoRoom.cs
//	Description:	Design and logic for the product owner room.
//  Course:         CSCI 4250-001, Software Engineering I
//	Author:			Jason Fields, fieldsja1@etsu.edu, East Tennessee State University
//	Created:		March 6th, 2021
//	Copyright:		Jason Fields, 2021
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Windows.Forms;
using System.Diagnostics;

namespace Poggies
{

    /// <summary>
    /// creates product room form
    /// </summary>
    public partial class PoRoom : Form
    {

        public static ClientStatement cs { get; set; } //main list used
        public static float POScore { get; set; } //score for PO

        public int rightAnswers;
        public int numCorrect;
        public int numTasks;

        /// <summary>
        /// constructor
        /// </summary>
        public PoRoom()
        {
            InitializeComponent();
            rightAnswers = 0;
            numCorrect = 0;
            clientStatementDropDown.Items.Add("Client Statement 1");
            clientStatementDropDown.Items.Add("Client Statement 2");
        }

        /// <summary>
        /// Handles the Click event of the PoNextRoomButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void PoNextRoomButton_Click(object sender , EventArgs e)
        {
            this.Hide();
           
        }


        /// <summary>
        /// Handles the Click event of the ProcessButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void ProcessButton_Click(object sender , EventArgs e)
        {
            for (int i = 0; i < clientStatementBox.Items.Count; i++)
            {
                CheckState st = clientStatementBox.GetItemCheckState(i);
                if (st == CheckState.Checked)
                {
                    if (cs.Requirements[i].CorrectAnswer == "n" || cs.Requirements[i].CorrectAnswer == "f")
                        numCorrect++;
                }
                else
                {
                    if (cs.Requirements[i].CorrectAnswer == "x")
                        numCorrect++;
                }
            }

            POScore = DriverClass.CalcScore(numCorrect , cs.Requirements.Count , POScore); //calc score
            scoreBox.Text = POScore.ToString(); //set box

            for (int i = 0; i < cs.Requirements.Count; i++)         //loop through each requirement
            {
                if (cs.Requirements[i].CorrectString == null)       //if the line is not a correct requirement
                    cs.Requirements.RemoveAt(i);                    //remove said line
            }
            foreach (var item in cs.Requirements)                   //loop through remaining items
            {
                correctStoriesBox.Items.Add(item.Name);             //add those items to the list box
            }

            clientStatementDropDown.Enabled = false;
            ProcessButton.Enabled = false;
            assignText.Enabled = true;
            correctStoriesBox.SelectedIndex = 0;

        }

        /// <summary>
        /// Handles the SelectedIndexChanged event of the CorrectStoriesBox control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void CorrectStoriesBox_SelectedIndexChanged(object sender , EventArgs e)
        {
            assignText.Clear();
        }

        /// <summary>
        /// Handles the TextChanged event of the AssignText control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void AssignText_TextChanged(object sender , EventArgs e)
        {
            if (assignText.Text.ToLower() == "f" || assignText.Text.ToLower() == "n")    //if text box is either 'f' or 'n'
            {

                int index = correctStoriesBox.SelectedIndex;        //set index position

                if (cs.Requirements[index].CorrectAnswer == assignText.Text)     //if the user answer is correct
                {
                    rightAnswers += 1;

                    if (index + 1 < cs.Requirements.Count)               //if the last position do not go to next position
                    {
                        correctStoriesBox.SelectedIndex = index + 1;
                    } //end if
                    else
                    {
                        assignText.Clear();
                        scoreBox.Text = DriverClass.CalcScore(rightAnswers , cs.Requirements.Count , POScore).ToString(); //get score
                        assignText.Enabled = false;
                        poNextRoomButton.Enabled = true;

                    } //end else


                } //end if
                else
                {
                    assignText.Clear();

                    if (index + 1 < cs.Requirements.Count)               //if the last position do not go to next position
                    {
                        correctStoriesBox.SelectedIndex = index + 1;
                    } //end if
                    else
                    {
                        assignText.Clear();
                        scoreBox.Text = DriverClass.CalcScore(rightAnswers , cs.Requirements.Count , POScore).ToString(); //get score
                        assignText.Enabled = false;
                        poNextRoomButton.Enabled = true;

                    } //end else

                }
            } //end if
            else //wrong input
            {
                if (assignText.Text != "")
                    MessageBox.Show("Invalid Input. Please enter 'f' or 'n'.");
                assignText.Clear();
            }
        }

        /// <summary>
        /// handles the form closing
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PoRoom_FormClosed(object sender , FormClosedEventArgs e)
        {
            Application.Exit();
        }

        /// <summary>
        /// handles the form closing
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PoRoom_FormClosing(object sender , FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                switch (MessageBox.Show("Are you sure you want to exit?\nYou will lose all progress" , "Warning!" , MessageBoxButtons.YesNo))
                {
                    case DialogResult.Yes:
                        Application.Exit();
                        break;
                    case DialogResult.No:
                        e.Cancel = true;
                        break;
                }
            }
        }

        private void ClientStatementDropDown_SelectedIndexChanged(object sender , EventArgs e)
        {
            clientStatementBox.Items.Clear();
            cs = new ClientStatement(clientStatementDropDown.SelectedIndex);
            foreach (var item in cs.Requirements)
            {
                clientStatementBox.Items.Add(item.Name);        //fill list check box with client information
            }

            if (clientStatementBox.Items.Count != 0)
            {
                ProcessButton.Enabled = true;       
            }
           
        }
    }
}
