﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//	Project:		Sprint1
//	File Name:		Developer.cs
//	Description:	Class to track developer properties
//  Course:         CSCI 4250-001, Software Engineering I
//	Author:			Edgar Guerra, guerrae@etsu.edu, East Tennessee State University
//	Created:		March 7th, 2021
//	Copyright:		Edgar Guerra, 2021
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poggies
{
    /// <summary>
    /// A class to represent a Developer Object
    /// </summary>
    public class Developer
    {
        // stats saved for the developer
        public int skillLvl { get; set; }
        public int experienceLvl { get; set; }
        public List<Requirement> myTask { get; set; }
        public int devID { get; set; }

        /// <summary>
        /// developer constructor
        /// </summary>
        /// <param name="skill">skill level</param>
        /// <param name="experience">experience level</param>
        /// <param name="id">id of the developer</param>
        public Developer(int skill, int experience, int id)
        {
            this.skillLvl = skill;
            this.experienceLvl = experience;
            this.devID = id;
            this.myTask = new List<Requirement>();
        }

        /// <summary>
        /// to string override
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return $"Developer ID: {devID}  Skill LVL: {skillLvl}   EXP LVL: {experienceLvl}";
        }
    }
}
