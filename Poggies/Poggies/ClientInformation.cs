﻿using System;

namespace Sprint1
{
    public class ClientInformation
    {
        public static string clientMessage()
        {
            string message = "";

            Random randomClientMessage = new Random();

            int rand_num = randomClientMessage.Next(0 , 10);

            if (rand_num >= 0 && rand_num < 3)
            {
                message = "Message One";
            }
            else if (rand_num >= 3 && rand_num < 6)
            {
                message = "Message Two";
            }
            else if (rand_num >= 6 && rand_num < 11)
            {
                message = "Message Three";
            }

            return message;
        }
    }
}
