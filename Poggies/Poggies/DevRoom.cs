﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//	Project:		Sprint1
//	File Name:		DevRoom.cs
//	Description:	Design and logic for the developer
//	Author:			Jason Fields, fieldsja1@etsu.edu, East Tennessee State University
//	Created:		March 6th, 2021
//	Copyright:		Jason Fields, 2021
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Poggies
{

    /// <summary>
    /// creates developer room form
    /// </summary>
    public partial class DevRoom : Form
    {
        private string[] remainingRooms;
        public int nextRoom, current;
        public static float DEVScore { get; set; }
        public static int Completed { get; set; }

        /// <summary>
        /// Checks to see if the task was completed
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void TaskCompleteCheck_Click(object sender , EventArgs e)
        {
            if (remainingRooms[current] != "")       //if the room is completed null it out to prevent the player from returning
            {               //THIS WILL NEED TO BE CHANGED TO MEET THE COMPLETED STATUS WHATEVER THAT MAY BE
                            //THIS IS JUST TO DEMONSTRATE FUNCTIONALITY
                remainingRooms[current] = "";
                Completed++;
                MessageBox.Show("Task Completed, select another room" , "Success");
                remainingRoomsBox.DataSource = null;
                remainingRoomsBox.Items.Clear();
                remainingRoomsBox.DataSource = remainingRooms;
            } //end if

            else
            {
                MessageBox.Show("Task not complete" , "Try again");
            } //end else

        }

        /// <summary>
        /// Handles the Tick event of the sprintTimer control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void sprintTimer_Tick(object sender , EventArgs e)
        {
            // try to go to next sprint.

            // if the is the last sprint
            //  show final score
            // else 
            //  start next sprint.
        }

        /// <summary>
        /// Initializes a new instance dev room
        /// </summary>
        /// <param name="tasks">The tasks.</param>
        /// <param name="currentTask">The current task.</param>
        /// <param name="remaining">The remaining.</param>
        /// <param name="completed">The completed.</param>
        public DevRoom(int currentTask , string[] remaining , int completed)
        {
            InitializeComponent();
            current = currentTask;      //sets the current task to track which room we are in
            roomTask.Text = PoRoom.cs.Requirements[currentTask].CorrectString;      //print the task information to the text box
            remainingRooms = remaining;
            remainingRoomsBox.DataSource = remainingRooms;
            DevRoom.Completed = completed;
        }

        private void DoneButton_Click(object sender , EventArgs e)
        {
            DEVScore = DriverClass.CalcScore(Completed , PoRoom.cs.Requirements.Count , DEVScore);
            this.Dispose();
            EndRoom endRoom = new EndRoom(remainingRooms);           //open and show the ending room    
            endRoom.Show();
        }

        private void DevCountdownTimer_Tick(object sender , EventArgs e)
        {
            if (DriverClass.time > 0)
            {
                DriverClass.time--;
                countdownDisplay.Text = DriverClass.time.ToString();
            }
            else
            {
                devCountdownTimer.Stop();
                countdownDisplay.Text = "Time's up!";
                MessageBox.Show("Your time is up and the current sprint is over\n" +
                    "You will now be taken to the end of sprint screen.", "Sprint Over");
                DEVScore = DriverClass.CalcScore(Completed , PoRoom.cs.Requirements.Count , DEVScore);
                this.Dispose();
                EndRoom endRoom = new EndRoom(remainingRooms);           //open and show the ending room    
                endRoom.Show();
            }

        }

        private void DevRoom_FormClosing(object sender , FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                switch (MessageBox.Show("Are you sure you want to exit?\nYou will lose all progress" , "Warning!" , MessageBoxButtons.YesNo))
                {
                    case DialogResult.Yes:
                        Application.Exit();
                        break;
                    case DialogResult.No:
                        e.Cancel = true;
                        break;
                }
            }
        }


        /// <summary>
        /// Handles the Click event of the Button1 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void Button1_Click(object sender , EventArgs e)
        {
            if (remainingRoomsBox.SelectedItem.ToString() != "")
                nextRoom = Int32.Parse(remainingRoomsBox.SelectedItem.ToString()) - 1;
            else
            {
                MessageBox.Show("Room already completed" , "Error");      //room already been completed error message
                return;
            }


            if (nextRoom != current && nextRoom >= 0)
            {
                if (nextRoom >= 0)    //if the selected room is within the range of rooms left
                {

                    if (PoRoom.cs.Requirements[nextRoom] != null)                 //if the room has not already been completed
                    {
                        this.Dispose();                               //close this form
                        DevRoom dev = new DevRoom(nextRoom , remainingRooms , Completed);     //create and show a new instance of the dev room
                        dev.Show();                                         //with the new selected room and tasks remaining
                    }//end if
                    else
                    {
                        MessageBox.Show("Room already completed" , "Error");      //room already been completed error message
                    }//end else
                }//end if
                else
                {
                    MessageBox.Show("Room selected out of range" , "Error");      //room out of the range of rooms left error
                }//end else
            }//end if
            else
            {
                MessageBox.Show("Already in selected room" , "Error");      //room out of the range of rooms left error
            }//end else
        }//end method


    }
}
