using Microsoft.VisualStudio.TestTools.UnitTesting;
using Xunit;
using Poggies;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

namespace Tests
{
    [TestClass]
    public class UnitTest1
    {


        /// <summary>
        /// checks developer constructor 
        /// </summary>
        /// <param name="skill"></param>
        /// <param name="experience"></param>
        /// <param name="id"></param>
      
        [Theory]
        [InlineData(0, 1, 2)]
        [InlineData(1, 2, 3)]
        public void checkDevConstructor(int skill, int experience, int id)
        {
            //Arrange
            Developer dev;

            //Act
            dev = new Developer(skill, experience, id);

            //Assert
            Assert.AreEqual(dev.skillLvl, skill);
            Assert.AreEqual(dev.experienceLvl, experience);
            Assert.AreEqual(dev.devID, id);

        }

        /// <summary>
        /// checks constructor of requirement class.
        /// </summary>
        /// <param name="line"></param>
       
        [Theory]
        [InlineData("Hey man, I need something from you and your team.x")]
        public void checkRequirementConstructor(string line)
        {
            //Arrange
            Requirement rq;

            //Act
            rq = new Requirement(line);

            //Assert
            Assert.AreEqual("Hey man, I need something from you and your team.", rq.Name);
            Assert.AreEqual("x", rq.CorrectAnswer);
            Assert.AreEqual(null, rq.MyAnswer);

        }

        /// <summary>
        /// checks for statement and decision coverage in requirement. In conjunction with successful output from previous test, this will ensure 
        /// both decision trees are triggered.
        /// </summary>
        /// <param name="line"></param>

        [Theory]
        [InlineData("Hey man, I need something from you and your team.f")]
        public void checkRequirementCoverage(string line)
        {
            //Arrange
            Requirement rq;

            //Act
            rq = new Requirement(line);

            //Assert
            Assert.AreEqual("Hey man, I need something from you and your team.", rq.Name);
            Assert.AreEqual("f", rq.CorrectAnswer);
            Assert.AreEqual(null, rq.MyAnswer);

        }

        /// <summary>
        /// This test also checks for statement coverage in the driver class because the total changes from 0 to something greater than 0
        /// so all if statements fire. Did not pass negative number because program does not allow. 
        /// </summary>
        /// <param name="right"></param>
        /// <param name="total"></param>
        /// <param name="final"></param>

        [Theory]
        [InlineData(0, 1, 2)]
        [InlineData(1, 2, 0)]
        [InlineData(2, 0, 1)]
        public void checkCalculateScore(int right, int total, int final)
        {
            //Arrange and Act
            float answer = DriverClass.CalcScore(right, total, final);

            //Assert
            if (right == 0)
            {
                Assert.AreEqual(1, answer);
            }
            else if (right == 1)
            {
                Assert.AreEqual(50, answer);
            }
            else if (right == 2)
            {
                Assert.AreEqual(1, answer);
            }
        }

        ///cannot test clientstatement class because of UnitTesting class not being able to access the file dialog (windows form)
        ///planned to test constructor so that index passed does indeed grab the correct file
        ///also planned to test for statement, decision, and loop coverage by
    }
}
