//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//	Project:		Sprint 1
//	File Name:		Requirement.cs
//	Description:	This class contains logic to create a requirement 
//	Course:			CSCI 4250-001 Software Engineering 1
//	Author:			Ian Grisham, grishami@etsu.edu, Department of Computing, East Tennessee State University
//	Created:		Sunday March 7, 2021
//	Copyright:		Ian Grisham, 2021
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////


namespace ConsoleApp1
{
    /// <summary>
    /// Requirement class holds each requirement and right/user answers
    /// </summary>
    class Requirement
    {
        public string MyAnswer { get; set; } //property for my answer
        public string CorrectAnswer { get; set; } //property for the correct answer to check against
        public string Name { get; set; } //property for the actual string

        /// <summary>
        /// constructor initializes
        /// </summary>
        /// <param name="name"></param>
        public Requirement(string name)
        {
            this.Name = name.Substring(0, name.Length - 1); //set this name to string minus last character
            this.CorrectAnswer = name[name.Length - 1].ToString(); //store hardcoded last character on line as answer 
            this.MyAnswer = null; //set this answer to null because user hasnt given answer yet
        }

    } //end requirement class
}
